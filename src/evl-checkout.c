/*	CVS-CHECKOUT command takes the SHA of commit unpacks it
*	and retreive the SHA of tree from it. Now it sends this 
*	SHA to another function where it unpacks this SHA of tree
* 	and retrives the SHA's of files in it. After retriving 
* 	files from it, it creates those files in our pwd and in 
*	short it moves us to our desired version. Now our pwd will
*	be containing all the files that were present in that version.
*/


#include"cache.h"

#define KNRM  "\x1B[0m"
#define KRED  "\x1B[31m"
#define KGRN  "\x1B[32m"
#define KYEL  "\x1B[33m"
#define KBLU  "\x1B[34m"
#define KMAG  "\x1B[35m"
#define KCYN  "\x1B[36m"
#define KWHT  "\x1B[37m"

/*
*	This is a helper functon which is used to unpack the tree struct
*/



static int unpack(unsigned char *sha1)
{
	void *buffer;
	unsigned long size;
	char type[20];
	/*	here we read an object file from .dircache/objects/ directory
	**	read_sha1_file() function defined in evl-read.c is used to read
	**	it returns the pointer to data present in memeory
	*/
	buffer = read_sha1_file(sha1, type, &size);
	// checking file readed or not
	if (!buffer)
		printf("%s\n\tunable to read sha1 file%s",KRED,KNRM);
	//checking tree type
	if (strcmp(type, "tree"))
		usage("expected a 'tree' node");
	//reading entries of file from buffer
	while (size) 
	{
		
		int len = strlen(buffer)+1;
		unsigned char *sha1 = buffer + len;
		char *path = strchr(buffer, ' ')+1;
		unsigned int mode;
		if (size < len + 20 || sscanf(buffer, "%o", &mode) != 1)
			usage("corrupt 'tree' file");
		buffer = sha1 + 20;
		size -= len + 20;
		char * path1 = path;
		char * fnd;
		while((fnd = strchr(path1,'/')) != NULL)
		{
			int siz1 = fnd - path1;
			char * dir = (char *)malloc(siz1+1);
			strncpy(dir,path1,siz1);
			dir[siz1] = NULL;
			mkdir(dir,0777);
			path1 = fnd+1;
		}
		int fd = open(path,O_CREAT|O_TRUNC|O_RDWR,0644);
		
		void * buf;
		unsigned long size12;
		buf = read_sha1_file(sha1, type, &size12);
		
		write(fd,buf,size12);
		

//		printf("%o %s (%s)\n", mode, path, sha1_to_hex(sha1));
	}
	return 0;
}




/*
*	This main function receives the SHA of your desired commit or it
* 	receives the HEAD if you want to move to the final version. There 
*	are two operations performed mainly in this file. One is unpacking
*	the SHA of tree and then unpacking and retrieving the files contained
*	in this SHA. Finally it creates new files and removes files of 
*	previous versions. 
*/


int main(int argc, char ** argv)
{
	
	if(argc <= 1)
	{
		printf("%s\nusage\n\tcvs checckout <commit-sha>\n%s",KRED,KNRM);
		exit(1);
	}
	char canCO;
	int fdcanCO = open(".evl/info/.modified",O_RDONLY);
	read(fdcanCO,&canCO,1);
/*	if(canCO == 'y')
	{
			printf("%s\n\tYou have intermediate changes. Please commit those changes\n\tThen checkout\n\n%s",KRED,KNRM);
			close(fdcanCO);
			exit(0);
	}*/
	close(fdcanCO);
	char * sha;
	if(strcmp(argv[1],"HEAD")==0)
	{
		char * path = malloc(strlen(DEFAULT_HEADS_DIR)+7);
		strcpy(path,DEFAULT_HEADS_DIR);
		strcpy(path+strlen(DEFAULT_HEADS_DIR),"/master");

		int fd = open(path,O_RDONLY);
		char hash[41];
		int k = read(fd,hash,40);
		if(k<0)
		{
			perror("Error : ");
		}
		hash[40] = NULL;
		sha = hash;
	}
	else
	{
		sha = argv[1];
	}

	void *buf;	
	char type[20];
	char sha1[20];

	unsigned long size;
	
	if (get_sha1_hex(sha, sha1))
		usage("invalid sha");

	buf = read_sha1_file(sha1, type, &size);

	
	char buf2[1000];
	bzero(buf2,1000); 
	char buf3[1000];
	bzero(buf3,1000);
	char * q;
	q = buf;
	int size1 =  size - (int)(q-(char *)buf);
//	printf("%d\n",size1);

	q+=5;
	size1 =  40;
//	printf("%d\n",size1);

//	printf(" %.7s\n",sha);
//	printf("%.*s",size1,++q);
	char treebuf[41];
	strncpy(treebuf,q,40);
	treebuf[40]=0;
	
	system("rm -r * 2>/dev/null 1>/dev/null");

	if (get_sha1_hex(treebuf, sha1) < 0)
		usage("read-tree <key>");
	
	if (unpack(sha1) < 0)
		usage("unpack failed");

	int fd2 = open(".evl/info/current.txt",O_TRUNC|O_RDWR|O_CREAT,0666);
	write(fd2,sha,40);
	close(fd2);
	unsigned char sha2[41];
	int fd1 = open(".evl/HEADS/master",O_RDONLY);
	int k=read(fd1,sha2,40);
	sha2[40]='\0';
	int fd3 = open(".evl/info/.canCommit",O_RDWR|O_TRUNC);
	if(strncmp(sha,sha2,40)==0)
	{
		write(fd3,"y",1);
	}
	else
	{
		write(fd3,"n",1);
	}
	close(fd3);
//	execlp("evl","evl","add",".",NULL);
	
	return 0;
}
