/*
*	This is a parent command which takes name of command 
*	as argument. It takes a command name and execute it.
*	if no argument is given it by default execute help command.
*/


#include<stdio.h> 
#include<stdlib.h>

#define KNRM  "\x1B[0m"
#define KRED  "\x1B[31m"
#define KGRN  "\x1B[32m"
#define KYEL  "\x1B[33m"
#define KBLU  "\x1B[34m"
#define KMAG  "\x1B[35m"
#define KCYN  "\x1B[36m"
#define KWHT  "\x1B[37m"


int main(int argc,char ** argv)
{
	/*	checking for no of arguments if 1 then be default execute help command	*/
	if(argc == 1)
	{
		char ** arglist = (char **)malloc(sizeof(char *)*2);
		arglist[0] = "evl-help";
		arglist[1] = NULL;
		execvp("evl-help",arglist);
	}
	/*	else check for the argument given and execute that	*/
	else
	{
		if(strcmp(argv[1],"help") == 0)
		{
			execvp("evl-help",++argv);
		}
		else if(strcmp(argv[1],"cat") == 0)
		{
			execvp("evl-cat",++argv);
		}
		else if(strcmp(argv[1],"diff") == 0)
		{
			execvp("evl-diff",++argv);
		}
		else if(strcmp(argv[1],"write-tree") == 0)
		{
			execvp("evl-write_tree",++argv);
		}
		else if(strcmp(argv[1],"read-tree") == 0)
		{
			execvp("evl-read_tree",++argv);
		}
		else if(strcmp(argv[1],"commit") == 0)
		{
			int cpid = fork();
			if(cpid == 0)
			{
				execvp("evl-write_tree",NULL);
			}
			while(wait(NULL)>0);
			execvp("evl-commit",++argv);
		}
		else if(strcmp(argv[1],"init") == 0)
		{
			execvp("evl-init",++argv);
		}
		else if(strcmp(argv[1],"log") == 0)
		{
			execvp("evl-log",++argv);
		}
		else if(strcmp(argv[1],"update") == 0)
		{
			execvp("evl-update",++argv);
		}
		else if(strcmp(argv[1],"add") == 0)
		{
			execvp("evl-add",++argv);
		}
		else if(strcmp(argv[1],"checkout") == 0)
		{
			execvp("evl-checkout",++argv);
		}
		else if(strcmp(argv[1],"co") == 0)
		{
			execvp("evl-checkout",++argv);
		}
		else if(strcmp(argv[1],"status") == 0)
		{
			execvp("evl-status",++argv);
		}
		else if(strcmp(argv[1],"ignore") == 0)
		{
			execvp("evl-ignore",++argv);
		}
		else if(strcmp(argv[1],"rm") == 0)
		{
			execvp("evl-rm",++argv);
		}
		/*	if no name match then display error	*/
		else
		{
			printf("\n\t%s%s is not a evl command%s\n\n",KRED,argv[1],KNRM);
		}
	}
	
	return 0;
}
