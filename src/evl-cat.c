#include "cache.h"

/*
* evl cat command takes one command line argument 
* That argument is SHA. And it converts this SHA to hex 
* After converting it to hex it reads the file containing SHA's
* and tell you type of SHA i.e., type commit and blob are dealt here.
* It shows you type of sha and then show content of that sha
*/

#define KNRM  "\x1B[0m"
#define KRED  "\x1B[31m"
#define KGRN  "\x1B[32m"
#define KYEL  "\x1B[33m"
#define KBLU  "\x1B[34m"
#define KMAG  "\x1B[35m"
#define KCYN  "\x1B[36m"
#define KWHT  "\x1B[37m"

int main( int argc , char **argv )
{
	unsigned char sha1[20];
	char type[20];
	void *buf;
	unsigned long size;

	if (argc != 2 || get_sha1_hex(argv[1], sha1))
	{
		printf("%s\n\nusage: evl cat <sha1 of commit/blob object>%s\n\n",KRED,KNRM);
		return 0;
	}
	buf = read_sha1_file(sha1, type, &size);
	
	if (!buf)
		exit(1);
	
	printf("\n%s%s\n%s\n", KGRN,type,KNRM);

	if (write(1, buf, size) != size)
		strcpy(type, "bad");

}
