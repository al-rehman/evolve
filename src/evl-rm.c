#include <cache.h>

static int cache_name_compare(const char *name1, int len1, const char *name2, int len2)
{
	int len = len1 < len2 ? len1 : len2;
	int cmp;

	cmp = memcmp(name1, name2, len);
	if (cmp)
		return cmp;
	if (len1 < len2)
		return -1;
	if (len1 > len2)
		return 1;
	return 0;
}

static int cache_name_pos(const char *name, int namelen)
{
	int first, last;

	first = 0;
	last = active_nr;
	while (last > first) {
		int next = (last + first) >> 1;
		struct cache_entry *ce = active_cache[next];
		int cmp = cache_name_compare(name, namelen, ce->name, ce->namelen);
		if (!cmp)
			return -next-1;
		if (cmp < 0) {
			last = next;
			continue;
		}
		first = next+1;
	}
	return first;
}


static int remove_file_from_cache(char *path)
{
	int pos = cache_name_pos(path, strlen(path));
	if (pos < 0) {
		pos = -pos-1;
		active_nr--;
		if (pos < active_nr)
			memmove(active_cache + pos, active_cache + pos + 1, (active_nr - pos - 1) * sizeof(struct cache_entry *));
	}
}

static int write_cache(int newfd, struct cache_entry **cache, int entries)
{
	SHA_CTX c;
	struct cache_header hdr;
	int i;

	hdr.signature = CACHE_SIGNATURE;
	hdr.version = 1;
	hdr.entries = entries;

	SHA1_Init(&c);
	SHA1_Update(&c, &hdr, offsetof(struct cache_header, sha1));
	for (i = 0; i < entries; i++) {
		struct cache_entry *ce = cache[i];
		int size = ce_size(ce);
		SHA1_Update(&c, ce, size);
	}
	SHA1_Final(hdr.sha1, &c);

	if (write(newfd, &hdr, sizeof(hdr)) != sizeof(hdr))
		return -1;

	for (i = 0; i < entries; i++) {
		struct cache_entry *ce = cache[i];
		int size = ce_size(ce);
		if (write(newfd, ce, size) != size)
			return -1;
	}
	return 0;
}		

int main(int argc, char **argv)
{
	int newfd, entries;

	entries = read_cache();

	if (entries < 0) {
		perror("cache corrupted");
		return -1;
	}

	newfd = open(".evl/index.lock", O_RDWR | O_CREAT | O_EXCL, 0600);
	if (newfd < 0) 
	{
		perror("unable to create new cachefile");
		return -1;
	}
	int i;
	for( i=1;i<argc;i++)
	{
		int len1 = strlen(argv[i]);
//		write(1,argv[i],len1);
		remove_file_from_cache(argv[i]);
//		write(1,argv[i],len1);

//		write(1,argv[i],len1);
		char * rmv = (char *)malloc(len1+4);
//		write(1,argv[i],len1);
		strncpy(rmv,"rm ",4);
//		write(1,argv[i],len1);
		strncpy(rmv+3,argv[i],len1);
//		write(1,argv[i],len1);
		system(rmv);
//		write(1,argv[i],len1);
	}
	

	if ( i>1 && !write_cache(newfd, active_cache, active_nr) && !rename(".evl/index.lock", ".evl/index"))
	{
		int fd3 = open(".evl/info/.modified",O_RDWR);
		write(fd3,"y",1);
		return 0;
	}
	return 0;
}



