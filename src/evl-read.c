/*	This file contains the helper function for reading and writing
**	to cache and object file these are the basic function which compress
**	and add data data to index file and data base.
*/


#include "cache.h"

/*	these variables are global variables defined in cache.h they are initializing
**	and populating so that other function use it.
*/

const char *sha1_file_directory = NULL;
struct cache_entry **active_cache = NULL;
unsigned int active_nr = 0, active_alloc = 0;


//	this is helper function to print error
void usage(const char *err)
{
	fprintf(stderr, "evl read-tree: %s\n", err);
	exit(1);
}


//	this helper function get char and return its hex value
//	e.g. if we have '8' its hex value in 4 bits is 8, if 'a' or 'A' it is 10.
static unsigned hexval(char c)
{
	if (c >= '0' && c <= '9')
		return c - '0';
	if (c >= 'a' && c <= 'f')
		return c - 'a' + 10;
	if (c >= 'A' && c <= 'F')
		return c - 'A' + 10;
	return ~0;
}



/*	this function takes hex and returns its corresponding in char * sha1
**	we input two char buffers one is hex which is 40 bytes hex characters string 
**	second is SHA1 it is 20 bytes not initialized at any value.
*/

int get_sha1_hex(char *hex, unsigned char *sha1)
{
	int i;
	for (i = 0; i < 20; i++) 
	{
		/*	for two bytes in hex calculating corresponding hex values and ORing
		**	them to make one compete value	*/
		unsigned int val = (hexval(hex[0]) << 4) | hexval(hex[1]);
		if (val & ~0xff)
			return -1;
		//	Copying the value in SHA1 and incrementing it
		*sha1++ = val;
		hex += 2;
	}
	return 0;
}

/*	this function is used to convert sha into hex 
*/

char * sha1_to_hex(unsigned char *sha1)
{
	static char buffer[50];
	/*	this array is used to determine which hex character will be placed in buffer	*/
	static const char hex[] = "0123456789abcdef";

	char *buf = buffer;
	int i;
	
	
	/*	for each byte in sha1 making two new bytes which contain hex chahracters based on each four bits of SHA1	*/
	
	for (i = 0; i < 20; i++) 
	{
		unsigned int val = *sha1++;
	
		/*	placing hex characters in buffer according to each byte first four bit in sha1 	*/
		*buf++ = hex[val >> 4];
	
		/*	placing hex characters in buffer according to each byte last four bit in sha1 	*/
		*buf++ = hex[val & 0xf];
	}
	return buffer;
}


/*	this function is used to give full name with path to the file that we have
**	to create in a object database. its format is
**		.evl/objects/--/--------------------------------------
**		.evl/objects/2 hex char/38 hex char
*/
char *sha1_file_name(unsigned char *sha1)
{
	int i;
	static char *name, *base;

	if (!base) {
		
		//	Getting the path of directory where our database exist which is .evl/objects
		char *sha1_file_directory =  DEFAULT_DB_ENVIRONMENT;

		int len = strlen(sha1_file_directory);
		
		//	allocating space to base which will be containing complete path of file/object in database
		base = malloc(len + 60);
		
		//	copying path to base 
		memcpy(base, sha1_file_directory, len);
		
		//	Setting zero on base onwords from base path
		memset(base+len, 0, 60);
		//	copying '/' to base after database name to make path
		base[len] = '/';
		//	copying '/' after 3 bytes because we know two charaters are for directory path
		base[len+3] = '/';
		//	name will be containing address where we start placing the Hex  characters
		name = base + len + 1;
	}
	for (i = 0; i < 20; i++) 
	{
		static char hex[] = "0123456789abcdef";
		unsigned int val = sha1[i];
		
		// 	pos will be address where we are going to place two hex characters
		//	that will be base + 0 + 0 if i=0; and base + i*2 + 1 if i > 0; 
		//	because we have to skip one byte for '/' after two bytes 
		char *pos = name + i*2 + (i > 0);
		
		*pos++ = hex[val >> 4];
		*pos = hex[val & 0xf];
	}
	return base;
}


/*
**	given an sha this function read its corresponding file from database
**	and read its type and size and return pointer to file content loaded 
**	in memory in uncompressed form.
*/
void * 
read_sha1_file(unsigned char *sha1, char *type, unsigned long *size)
{
	z_stream stream;

	char buffer[8192];

	struct stat st;

	int i, fd, ret, bytes;

	void *map, *buf;

	/*	Getting a name of file with path associated to database	*/
	char *filename = sha1_file_name(sha1);
	
	fd = open(filename, O_RDONLY);
	if (fd < 0) 
	{
		perror(filename);
		return NULL;
	}
	
	//	Reading stats of file 
	if (fstat(fd, &st) < 0) 
	{
		close(fd);
		return NULL;
	}
	
	
	//	mapping file on hard and copying it to Memory
	map = mmap(NULL, st.st_size, PROT_READ, MAP_PRIVATE, fd, 0);
	close(fd);
	if (-1 == (int)(long)map)
		return NULL;
	
	//	setting stream to 0's
	memset(&stream, 0, sizeof(stream));

	//	setting stream next in value and size
	stream.next_in = map;
	stream.avail_in = st.st_size;
	//	setting the next out value and size
	stream.next_out = buffer;
	stream.avail_out = sizeof(buffer);
	
	//	uncompressing the content in stream
	inflateInit(&stream);
	ret = inflate(&stream, 0);
	
	if (sscanf(buffer, "%10s %lu", type, size) != 2)
		return NULL;
	bytes = strlen(buffer) + 1;
	buf = malloc(*size);
	if (!buf)
		return NULL;
	
	//	copying the content of file to buf as the buffer contain the [	type size_of_file'\0'content...	]
	memcpy(buf, buffer + bytes, stream.total_out - bytes);
	
	//	remaining bytes still uncompressed	
	bytes = stream.total_out - bytes;
	
	//	if remaining bytes is less then size means we have capacity for occupying the uncompreesed content
	if (bytes < *size && ret == Z_OK) 
	{
		stream.next_out = buf + bytes;
		stream.avail_out = *size - bytes;
		while (inflate(&stream, Z_FINISH) == Z_OK);
	}
	inflateEnd(&stream);
	//	retruning a address of content of file in memory in uncompressed form
	return buf;
}



/*	this function is used to write a given buffer and size of buffer to database
**	It firstly compress the content and calculates it SHA 
*/
int 
write_sha1_file(char *buf, unsigned len)
{
	int size;
	char *compressed;
	
	//	for compressing content of file
	z_stream stream;
	
	unsigned char sha1[20];
	
	//	for making unique SHA1 hash of compressed data
	SHA_CTX c;

	memset(&stream, 0, sizeof(stream));
	deflateInit(&stream, Z_BEST_COMPRESSION);
	size = deflateBound(&stream, len);
	compressed = malloc(size);

	stream.next_in = buf;
	stream.avail_in = len;
	stream.next_out = compressed;
	stream.avail_out = size;
	while (deflate(&stream, Z_FINISH) == Z_OK);

	deflateEnd(&stream);
	size = stream.total_out;

	SHA1_Init(&c);
	SHA1_Update(&c, compressed, size);
	SHA1_Final(sha1, &c);

	if (write_sha1_buffer(sha1, compressed, size) < 0)
		return -1;
	if(write_commit==1)
	{
		int fd=open(".evl/.commit_hash.txt",O_APPEND|O_CREAT|O_RDWR,0666);
		int fd1=open(".evl/HEADS/master",O_RDWR);
		int fd2 = open(".evl/info/current.txt",O_TRUNC|O_RDWR|O_CREAT,0666);
		char bufsha[250];
		sprintf(bufsha,"%s\n",sha1_to_hex(sha1));
		bufsha[41]='\0';
		write(fd,&bufsha,40);
		write(fd1,&bufsha,40);
		write(fd2,&bufsha,40);
	}
	else
	{
		int fd=open(".evl/.tree_hash.txt",O_CREAT|O_RDWR,0666);
		char bufsha[250];
		sprintf(bufsha,"%s\n",sha1_to_hex(sha1));
		bufsha[41]='\0';
		write(fd,&bufsha,40);
		
	}
	return 0;
}

/*	Given a compressed data, SHA and size of data write data to file 
**	it firstly create file with calculated SHA and write the compressed 
**	contents to file.
*/
int 
write_sha1_buffer(unsigned char *sha1, void *buf, unsigned int size)
{
	char *filename = sha1_file_name(sha1);
	int i, fd;

	fd = open(filename, O_WRONLY | O_CREAT | O_EXCL, 0666);
	if (fd < 0)
		return (errno == EEXIST) ? 0 : -1;
	write(fd, buf, size);
	close(fd);
	return 0;
}

/*	To Show error 
*/
static int error(const char * string)
{
	fprintf(stderr, "error: %s\n", string);
	return -1;
}

/*	While reading a index file we firstly verify its header if it is 
**	corrupted or not. this function help us to verify its header.
**	Given a cache header we firstly check signature and and version
**	than recalculate the SHA of content and check with previous SHA.
*/
static int verify_hdr(struct cache_header *hdr, unsigned long size)
{
	SHA_CTX c;
	unsigned char sha1[20];

	if (hdr->signature != CACHE_SIGNATURE)
		return error("bad signature");
	if (hdr->version != 1)
		return error("bad version");
	SHA1_Init(&c);
	SHA1_Update(&c, hdr, offsetof(struct cache_header, sha1));
	SHA1_Update(&c, hdr+1, size - sizeof(*hdr));
	SHA1_Final(sha1, &c);
	if (memcmp(sha1, hdr->sha1, 20))
		return error("bad header sha1");
	return 0;
}



/*	This function reads already present entries of files from stagging are which
**	is our index file. it firstly read its header and verify the content of index
**	than it will update the active_cache the global defined variable with presently
**	available entries of file in a index file and finally return the number of entries.
*/
int read_cache(void)
{
	int fd, i;
	struct stat st;
	unsigned long size, offset;
	void *map;
	struct cache_header *hdr;

	errno = EBUSY;
	if (active_cache)
		return error("more than one cachefile");
	errno = ENOENT;
	sha1_file_directory = getenv(DB_ENVIRONMENT);
	if (!sha1_file_directory)
		sha1_file_directory = DEFAULT_DB_ENVIRONMENT;
	if (access(sha1_file_directory, X_OK) < 0)
		return error("no access to SHA1 file directory");
	fd = open(".evl/index", O_RDONLY);
	if (fd < 0)
		return (errno == ENOENT) ? 0 : error("open failed");

	map = (void *)-1;
	if (!fstat(fd, &st)) {
		map = NULL;
		size = st.st_size;
		errno = EINVAL;
		if (size > sizeof(struct cache_header))
			map = mmap(NULL, size, PROT_READ, MAP_PRIVATE, fd, 0);
	}
	close(fd);
	if (-1 == (int)(long)map)
		return error("mmap failed");

	hdr = map;
	if (verify_hdr(hdr, size) < 0)
		goto unmap;

	active_nr = hdr->entries;
	active_alloc = alloc_nr(active_nr);
	active_cache = calloc(active_alloc, sizeof(struct cache_entry *));

	offset = sizeof(*hdr);
	for (i = 0; i < hdr->entries; i++) {
		struct cache_entry *ce = map + offset;
		offset = offset + ce_size(ce);
		active_cache[i] = ce;
	}
	return active_nr;

unmap:
	munmap(map, size);
	errno = EINVAL;
	return error("verify header failed");
}

