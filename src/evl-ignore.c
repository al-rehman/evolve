#include "cache.h"

/*
*	This commands takes names or regular expressions
*	of names from user and put them in file. Whenever 
*	that file appears in cvs-git command it refrains it 
*	from adding it to cache. These ignored file will 
*	not be tracked. If you dont want to put some special
*	sort of files in your repository you can put their 
*	names in this file using this command.
*/

int main(int argc, char ** argv)
{
	if(argc <= 1)
	{
		printf("invalid number of arguments\n\tusage cvs ignore <filename/regex>...");
		exit(1);
	}
	int fd = open(".evl/info/exclude",O_CREAT|O_APPEND|O_RDWR,0666);
	if(fd<0)
	{
		printf("Error opening file\n");
		exit(1);
	}
	int i;
	for( i = 0; i < argc-1 ; i++ )
	{
		write(fd,argv[i+1],strlen(argv[i+1]));
		write(fd,"\n",1);
	}
}
