/*
*	This command takes file name(s) or directory name as 
*	as argument. If it has received a directory name it 
*	opens the directory fetches the file(s) from it	and
* 	then finally add them to the stagging area. If it has
*	received file name(s) then simply adds those file(s)
* 	to stagging area.
*/

#include "cache.h"
#include<dirent.h>
#include<stdlib.h>
#include<sys/wait.h>
#include<sys/types.h>
#include<regex.h>
#include<string.h>
#include<unistd.h>
#include<stdbool.h>

#define KNRM  "\x1B[0m"
#define KRED  "\x1B[31m"
#define KGRN  "\x1B[32m"
#define KYEL  "\x1B[33m"
#define KBLU  "\x1B[34m"
#define KMAG  "\x1B[35m"
#define KCYN  "\x1B[36m"
#define KWHT  "\x1B[37m"


/*
*	If it receives file name(s) then simply adds those
* 	file(s) to stagging area after making arguments list.
*/
void add_file_to_cache(char * file);

/*
*	It is called by both add_file_to_cache() and void add_dir_to_cache()
*	and they pass arguments list in it and this funtion updates the 
*	stagging area.	
*/
void call_evl_update(char ** arglist);

/*
*	If it receives a directory name it opens the directory  
*	fetches the file(s) from it and then finally add them 
*	to the stagging area after making arguments list.
*/
void add_dir_to_cache(DIR * fdir,char *);

void show(char ** arg, int count);

int main(int argc, char ** argv)
{
	if(argc <= 1)
	{
		printf("%s\n\tUsage evl add <directory/file name>\n\n%s",KRED,KNRM);
		exit(1);
	}
	char canCommit[1];
	int fd4canAdd=open(".evl/info/.canCommit",O_RDWR);
	read(fd4canAdd,canCommit,1);
	if(canCommit[0] == 'n')
	{
		printf("%s\n\tCan not update cache at Detached HEAD state\n\n%s",KRED,KNRM);
		exit(0);
	}

	int i;
	for(i=1 ; i< argc ; i++ )
	{
		struct stat s;
//		printf("loop for each dir\n");
		int k = stat(argv[i],&s);
		if( k == -1 )
		{
			printf("\n\t%sCan not add file %s\n\n%s",KRED,argv[i],KNRM);
			exit(1);
		}
//		printf("%o\n", 0040000);
//		printf("%o\n", s.st_mode & 0170000);
//		printf("%d\n", (s)((int.st_mode & 0170000) == 0040000));


		if( (s.st_mode & 0170000) == 0040000 )
		{
			DIR * fdir;
			fdir = opendir(argv[i]);
			if(fdir == NULL)
			{
				perror("Eorror: ");
				exit(1);
			}
//			printf("adding dir to cache function calling\n");
			add_dir_to_cache(fdir,argv[i]);
		}
		else if( ( s.st_mode & 0170000) == 0100000)
		{
		
			
//			printf("In file mode section\n");
			add_file_to_cache(argv[i]);
		}
	}
	int fd3 = open(".evl/info/.modified",O_RDWR);
	write(fd3,"y",1);
	return 0;
}


bool strmatch(char str[], char pattern[],
              int n, int m)
{
    // empty pattern can only match with
    // empty string
    if (m == 0)
        return (n == 0);
 
    // lookup table for storing results of
    // subproblems
    bool lookup[n + 1][m + 1];
 
    // initailze lookup table to false
    memset(lookup, false, sizeof(lookup));
 
    // empty pattern can match with empty string
    lookup[0][0] = true;
 
    // Only '*' can match with empty string
    int j;
    for (j = 1; j <= m; j++)
        if (pattern[j - 1] == '*')
            lookup[0][j] = lookup[0][j - 1];
 
    // fill the table in bottom-up fashion
    int i;
    for (i = 1; i <= n; i++)
    {
    	int j;
        for (j = 1; j <= m; j++)
        {
            // Two cases if we see a '*'
            // a) We ignore ‘*’ character and move
            //    to next  character in the pattern,
            //     i.e., ‘*’ indicates an empty sequence.
            // b) '*' character matches with ith
            //     character in input
            if (pattern[j - 1] == '*')
                lookup[i][j] = lookup[i][j - 1] ||
                               lookup[i - 1][j];
 
            // Current characters are considered as
            // matching in two cases
            // (a) current character of pattern is '?'
            // (b) characters actually match
            else if (pattern[j - 1] == '?' ||
                    str[i - 1] == pattern[j - 1])
                lookup[i][j] = lookup[i - 1][j - 1];
 
            // If characters don't match
            else lookup[i][j] = false;
        }
    }
 
    return lookup[n][m];
}

int match(char * str)
{
	char msgbuf[100];
	int fd12 = open(".evl/info/exclude",O_RDONLY);
	if(fd12 > 0)
	{
		int n=0;
		FILE * fi = fdopen(fd12,"r");
		while((n=fgets(msgbuf,100,fi))!=NULL)
		{
			int l1 = strlen(msgbuf);
			msgbuf[l1-1] = NULL;
		//		printf("%s\n",msgbuf);
			if(strmatch(str,msgbuf,strlen(str),strlen(msgbuf)) == true)
			{
				return 1;
			}
		//		printf("iteration complete\n");
		}
	}
	return 0;
}


void add_file_to_cache(char * file)
{

	if(match(file) == 1)
		return;
//	printf(" add file to cache section\n");
	char ** arglist = (char **) malloc((sizeof(char*)*2));
	arglist[0] = (char *)malloc(strlen("evl-update"));
	strncpy(arglist[0],"evl-update",strlen("evl-update"));
	arglist[1] = (char *)malloc(strlen(file)+1);
	strncpy(arglist[1],file,strlen(file));
	arglist[1][strlen(file)] = NULL;
	call_evl_update(arglist);
}
void add_dir_to_cache(DIR * fdir,char*name)
{
//	printf("in function add dir to cache\n");
	char ** arglist;
	arglist = (char ** )malloc (sizeof(char *)*2);
	arglist[0] = "evl-update";
	int argcount = 2;
	struct dirent * d;
//	printf("in function add dir to cache while starting for each file\n");
	while((d=readdir(fdir))!=NULL)
	{
		if(d->d_name[0] != '.')
		{
			if(match(d->d_name)==1)
				continue;

			struct stat st;
			stat(d->d_name,&st);

			if( (st.st_mode&0170000) == 0040000)
			{
				continue;
			}

//			printf("in function add dir to cache while starting for each file %s\n",d->d_name);

			char * file1 = (char*)malloc(sizeof(char) * (strlen(d->d_name)+1+strlen(name)+1));
			if(strncmp(name,".",1) == 0)
			{
				strncpy(file1,d->d_name,strlen(d->d_name));
				file1[strlen(d->d_name)] = NULL;
			}
			else
			{
				strncpy(file1,name,strlen(name));
				file1[strlen(name)] = '/';
				strncpy(file1+strlen(name)+1,d->d_name,strlen(d->d_name));
				file1[strlen(d->d_name)+1+strlen(name)] = NULL;
			}
			arglist[argcount-1] = file1;

			argcount++;
			arglist = realloc(arglist,sizeof(char*)*argcount);
		}
	}

	arglist[argcount-1]=0;

//	show(arglist,argcount);

//	printf("in function add dir to cache calling evl update\n",d->d_name);
	call_evl_update(arglist);
}


void show(char ** arg, int count)
{
	int i;
	for(i=0 ;i< count-1; i++)
	{
		printf("%d - %s\n",i,arg[i]);
	}
}

void call_evl_update(char ** arglist)
{
//	printf("in function call evl update\n");
	int cpid = fork();
	if(cpid == 0)
	{
//		show(arglist,3);
//		printf("in function add dir cache calling execlp\n");

		execvp("evl-update",arglist);

	}
	else
	{
//		printf("in function call evl update calling wait\n");
		while(wait(NULL)>0);
	}
}

