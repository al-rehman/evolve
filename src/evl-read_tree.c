/*	This file contins the code for reading database objects of type tree
**	this file make up the command evl-read_tree which takes a SHA of tree as input
**	and read the object database of that SHA if it tree object it is displayed.
*/


#include "cache.h"

#define KNRM  "\x1B[0m"
#define KRED  "\x1B[31m"
#define KGRN  "\x1B[32m"
#define KYEL  "\x1B[33m"
#define KBLU  "\x1B[34m"
#define KMAG  "\x1B[35m"
#define KCYN  "\x1B[36m"
#define KWHT  "\x1B[37m"


//	This is a helper functon which is used to unpack the tree struct
static int unpack(unsigned char *sha1)
{
	void *buffer;
	unsigned long size;
	char type[20];
	/*	here we read an object file from .dircache/objects/ directory
	**	read_sha1_file() function defined in evl-read.c is used to read
	**	it returns the pointer to data present in memeory
	*/
	buffer = read_sha1_file(sha1, type, &size);
	// checking file readed or not
	if (!buffer)
		printf("%s\n\tERROR:  unable to read sha1 file\n%s",KRED,KNRM);
	//checking tree type
	if (strcmp(type, "tree"))
		printf("\n%s\texpected a 'tree' node%s\n",KRED,KNRM);
	//reading entries of file from buffer
	while (size) {
		int len = strlen(buffer)+1;
		unsigned char *sha1 = buffer + len;
		char *path = strchr(buffer, ' ')+1;
		unsigned int mode;
		if (size < len + 20 || sscanf(buffer, "%o", &mode) != 1)
			printf("%s\n\tERROR: corrupt 'tree' file%s\n",KRED,KNRM);
		buffer = sha1 + 20;
		size -= len + 20;
		printf("%o %s (%s%s%s)\n", mode, path, KBLU, sha1_to_hex(sha1),KNRM);
	}
	return 0;
}
//	this is main function which is called when we run command evl-read_tree <sha>
int main(int argc, char **argv)
{
	int fd;
	unsigned char sha1[20];

	if (argc != 2)
		usage("read-tree <key>");
	if (get_sha1_hex(argv[1], sha1) < 0)
		usage("read-tree <key>");
	sha1_file_directory = getenv(DB_ENVIRONMENT);
	if (!sha1_file_directory)
		sha1_file_directory = DEFAULT_DB_ENVIRONMENT;
	if (unpack(sha1) < 0)
		usage("unpack failed");
	return 0;
}
